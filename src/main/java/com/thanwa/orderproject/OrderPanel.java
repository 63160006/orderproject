/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.orderproject;

import java.awt.Button;
import java.awt.GridLayout;

/**
 *
 * @author tud08
 */
public class OrderPanel extends javax.swing.JPanel implements OnByProductListener{

    /**
     * Creates new form OrderPanel
     */
    public OrderPanel() {
        initComponents();
        productListPanel.setOrderPanel(this);
        productListPanel.addOnByListener(this);
        productListPanel.addOnByListener(sellPanel);
    }

    public void buy(Product product, int amount) {
        txtInfo.setText("Order Panel "+product+ " " + amount);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtInfo = new javax.swing.JLabel();
        productListPanel = new com.thanwa.orderproject.ProductListPanel();
        sellPanel = new com.thanwa.orderproject.SellPanel();

        setBackground(new java.awt.Color(0, 102, 102));

        txtInfo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtInfo.setForeground(new java.awt.Color(240, 240, 240));
        txtInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtInfo.setText("INFO");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(productListPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sellPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sellPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 77, Short.MAX_VALUE))
                    .addComponent(productListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.thanwa.orderproject.ProductListPanel productListPanel;
    private com.thanwa.orderproject.SellPanel sellPanel;
    private javax.swing.JLabel txtInfo;
    // End of variables declaration//GEN-END:variables

}
